package com.epam.controller;

import com.epam.model.Appliance;
import com.epam.model.BusinessLogic;
import com.epam.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<Appliance> getAllAppliances() {
        return model.getAllAppliances();
    }

    @Override
    public void add(Appliance appliance) {
        model.add(appliance);
    }

    @Override
    public boolean isNameUsed(String name) {
        return model.isNameUsed(name);
    }

    @Override
    public boolean delete(String name) {
        return model.delete(name);
    }

    @Override
    public void print() {
        model.print();
    }

    @Override
    public List<Appliance> turnedOnAppliances() {
        return model.turnedOnAppliances();
    }

    @Override
    public List<Appliance> sortedByPowerAppliances() {
        return model.sortedByPowerAppliances();
    }

    @Override
    public List<Appliance> appliancesFromRange(int bottom, int top) {
        return model.appliancesFromRange(bottom, top);
    }


}
