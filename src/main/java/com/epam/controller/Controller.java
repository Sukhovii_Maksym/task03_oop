package com.epam.controller;

import com.epam.model.Appliance;

import java.util.List;

public interface Controller {

    List<Appliance> getAllAppliances();

    void add(Appliance appliance);

    boolean isNameUsed(String name);

    boolean delete(String name);

    void print();

    List<Appliance> turnedOnAppliances();

    List<Appliance> sortedByPowerAppliances();

    List<Appliance> appliancesFromRange(int bottom, int top);
}
