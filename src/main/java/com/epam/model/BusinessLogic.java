package com.epam.model;

import java.util.List;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<Appliance> getAllAppliances() {
        return domain.getAllAppliances();
    }

    @Override
    public void add(Appliance applience) {
        domain.add(applience);
    }

    @Override
    public boolean isNameUsed(String name) {
        return domain.isNameUsed(name);
    }

    @Override
    public boolean delete(String name) {
        return domain.delete(name);
    }

    @Override
    public void print() {
        domain.print();
    }

    @Override
    public List<Appliance> turnedOnAppliances() {
        return domain.turnedOnAppliances();
    }

    @Override
    public List<Appliance> sortedByPowerAppliances() {
        return domain.sortedByPowerAppliances();
    }

    @Override
    public List<Appliance> appliancesFromRange(int bottom, int top) {
        return domain.appliancesFromRange(bottom, top);
    }
}
